#!/usr/bin/perl
#
# Copyright 2014 Johannes Schauer
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# srebuild uses sbuilds hook functionality (needs #774359 to be fixed) to
# install the right dependencies from a .buildinfo into the sbuild schroot.
#
# Current limitations:
#
#  - it will only search for results in Debian Sid, main

use strict;
use warnings;

use Archive::Tar;
use Cwd qw(abs_path);
use Dpkg::Control;
use Dpkg::Compression::FileHandle;
use Dpkg::Deps;
use Dpkg::Index;
use Dpkg::Checksums;
use DateTime::Format::Strptime;
use Compress::Zlib;
use File::Basename;
use File::Copy "cp";
use File::Temp qw(tempdir);
use Digest::SHA qw(sha256_hex);
use List::Util qw(first);

eval {
    require LWP::Simple;
    require LWP::UserAgent;
    no warnings;
    $LWP::Simple::ua =
      LWP::UserAgent->new( agent => 'LWP::UserAgent/srebuild' );
};
if ($@) {
    die
"Unable to run: Couldn't load LWP::Simple: $@ (is the libwww-perl package installed?)";
}

eval { require JSON; };
if ($@) {
    die
"Unable to run: Couldn't load JSON: $@ (is the libjson-perl package installed?)";
}

sub uniq {
    my %seen;
    return grep { !$seen{$_}++ } @_;
}

sub system_fatal {
    my @args = @_;
    print "srebuild: executing: @args\n";
    my $retval = system @args;
    $retval >>= 8;
    if ( $retval != 0 ) {
        die "failed: @args";
    }
}

# this subroutine is from debsnap(1)
sub fetch_json_page {
    my ($json_url) = @_;
    my $content = LWP::Simple::get($json_url);
    return unless defined $content;
    my $json      = JSON->new();
    my $json_text = $json->allow_nonref->utf8->relaxed->decode($content);
    return $json_text;
}

sub check_checksums {
    my $checksums = shift;

    foreach my $fname ( $checksums->get_files() ) {
        my $chksum = $checksums->get_checksum( $fname, 'sha256' );
        my $size   = $checksums->get_size($fname);
        my $size2  = ( stat($fname) )[7];
        if ( $size != $size2 ) {
            print "buildinfo: $size\n";
            print "actual:    $size2\n";
            die "size mismatch for $fname\n";
        }
        open my $fh, '<', $fname;
        my $chksum2 = sha256_hex <$fh>;
        close $fh;
        if ( $chksum ne $chksum2 ) {
            print "buildinfo: $chksum\n";
            print "actual:    $chksum2\n";
            die "checksum mismatch for $fname\n";
        }
    }
}

sub generate_in_toto_metadata {
    my $checksums = shift;

    my @files = $checksums->get_files();

    system_fatal "/usr/local/bin/in-toto-run", "--step-name=rebuild", "--gpg", "--products", @files,
        "--no-command";

    system_fatal "ls";

    my $lnk_file = (glob( "./rebuild.*.link" ))[0];
    die "could not find in-toto link metadata" unless defined ( $lnk_file );
    return abs_path( $lnk_file );
}

sub parse_buildinfo {
    my $buildinfo = shift;

    # the CTRL_FILE_CHANGES type should be closest to the .buildinfo format
    my $cdata = Dpkg::Control->new( type => CTRL_FILE_CHANGES );

    if ( not $cdata->load($buildinfo) ) {
        die "cannot parse";
    }

    my $depends = $cdata->{"Installed-Build-Depends"};
    die "need Installed-Build-Depends field" unless defined $depends;

    my $arch = $cdata->{"Build-Architecture"};
    die "need Build-Architecture field" unless defined $arch;

    my $checksums = Dpkg::Checksums->new();
    $checksums->add_from_control($cdata);
    if ( scalar $checksums->get_files() == 0 ) {
        die "need Checksums-* field";
    }

    my @depends = ();
    $depends =~ s/^\s+|\s+$//g;
    foreach my $dep ( split( /\s*,\s*/m, $depends ) ) {
        my $pkg = Dpkg::Deps::Simple->new($dep);
        die "name undefined" unless defined $pkg->{package};
        if ( defined( $pkg->{relation} ) ) {
            if ( $pkg->{relation} ne "=" ) {
                die "wrong relation";
            }
            die "version undefined" unless defined $pkg->{version};
        }
        else {
            die "no version";
        }
        push @depends,
          {
            name         => $pkg->{package},
            architecture => ( $pkg->{archqual} || $arch ),
            version      => $pkg->{version}
          };
    }

    return $cdata, $arch, $checksums, @depends;
}

sub get_first_seen {
    my $archive = shift;
    my $suite   = shift;
    my $area    = shift;
    my $arch    = shift;
    my $pkg     = shift;
    my $ver     = shift;
    my $url =
      "http://snapshot.debian.org/mr/binary/$pkg/$ver/binfiles?fileinfo=1";
    my $json_text = fetch_json_page($url);

    unless ( $json_text && @{ $json_text->{result} } ) {
        print STDERR "Unable to retrieve information for $pkg=$ver from $url.\n";
        return;
    }
    my $hash = undef;
    if ( scalar @{ $json_text->{result} } == 1 ) {
        if ( @{ $json_text->{result} }[0]->{architecture} ne "all" ) {
            print STDERR "expected arch:all\n";
            return;
        }
        $hash = ${ $json_text->{result} }[0]->{hash};
    }
    else {
        foreach my $result ( @{ $json_text->{result} } ) {
            if ( $result->{architecture} eq $arch ) {
                $hash = $result->{hash};
                last;
            }
        }
    }
    if ( not defined($hash) ) {
        print STDERR "cannot find architecture for $pkg=$ver\n";
        return;
    }
    my @first_seen = grep { $_->{archive_name} eq $archive }
      @{ $json_text->{fileinfo}->{$hash} };
    if ( scalar @first_seen != 1 ) {
        print STDERR "more than one package with the same hash\n";
        return;
    }
    @first_seen = map { $_->{first_seen} } @first_seen;
    return $first_seen[0];
}

sub setup_chroot_sbuild {
    my $base_repo = shift;
    my $suite   = shift;
    my $area    = shift;
    my $arch    = shift;
    my $src_pkg = shift;
    my $src_ver = shift;
    my @repos   = @_;

    # my $build_root = $ENV{'SBUILD_CHROOT_DIR'};
    my $build_root = abs_path("./$src_pkg-$src_ver");
    die "need chroot path" unless defined $build_root;
    my $bn_build_root = basename $build_root;

    @repos = map { "--extra-repository=$_" } @repos;

    unlink glob "/etc/schroot/chroot.d/$suite-$bn_build_root-$arch-sbuild-*";
    # Setup chroot
    say STDOUT "Extracting chroot";
    system_fatal "mkdir", "--parents", "$build_root";
    system_fatal "sbuild-createchroot",
      "--alias=$bn_build_root", "--chroot-prefix=$suite-$bn_build_root", @repos,
      "$suite", "$build_root",
      "$base_repo";
    say STDOUT "Done extracting chroot";
    return $build_root;
}

sub filter_depends {
    my $base_repo = shift(@_);
    my $suite = shift(@_);
    my $area = shift(@_);
    my $arch = shift(@_);
    my %reqpkgs = ();
    foreach my $pkg (@_) {
        $reqpkgs{"$pkg->{name}:$pkg->{architecture}=$pkg->{version}"} = $pkg;
    }
    my $snapshot_url = "$base_repo/dists/$suite/$area/binary-$arch/Packages.gz";
    my $response = LWP::Simple::get($snapshot_url);
    my $dest = Compress::Zlib::memGunzip($response)
        or die "Cannot uncompress\n";

    print STDERR "process Packages.gz\n";

    open my $fh, '<', \$dest;

    while (1) {
        my $cdata = Dpkg::Control->new(type => CTRL_INDEX_PKG);
        last if not $cdata->parse($fh, "Packages.gz");
        my $pkgname = $cdata->{"Package"};
        next if not defined($pkgname);
        my $pkgver = $cdata->{"Version"};
        my $pkgarch;
        if ($cdata->{"Architecture"} eq "all") {
            $pkgarch = $arch;
        } else {
            $pkgarch = $cdata->{"Architecture"};
        }
        my $key = "$pkgname:$pkgarch=$pkgver";
        if (exists $reqpkgs{$key}) {
            delete $reqpkgs{$key};
        }
    }
    close $fh;

    my @depends = values %reqpkgs;
    return @depends;
}

sub generate_sources {
    my $archive   = shift(@_);
    my $suite     = shift(@_);
    my $area      = shift(@_);
    my $arch      = shift(@_);
    my $builddate = shift(@_);

    say STDERR "retrieve last seen snapshot timestamps for each dependency";
    my @timestamps = ();

    my $dtparser = DateTime::Format::Strptime->new(
        pattern  => '%Y%m%dT%H%M%SZ',
        on_error => 'croak',
    );

    foreach my $pkg (@_) {
        my $first_seen =
          get_first_seen( $archive, $suite, $area, $arch, $pkg->{name},
            $pkg->{version} );
        die "" unless defined $first_seen;
        push @timestamps, $dtparser->parse_datetime($first_seen);
        print "Done $pkg->{name}=$pkg->{version}\n";
    }

    @timestamps = sort @timestamps;
    @timestamps = uniq(@timestamps);

    @timestamps = grep { DateTime->compare( $_, $builddate ) != 1 } @timestamps;
    @timestamps = map  { $_->strftime("%Y%m%dT%H%M%SZ") } @timestamps;
    @timestamps = map {
        "deb    http://snapshot.debian.org/archive/$archive/$_/    $suite $area"
    } @timestamps;

    return @timestamps;
}

my $archive = "debian";
my $suite   = "sid";
my $area    = "main";

my $buildinfo = shift @ARGV;
if ( not defined($buildinfo) ) {
    die "need buildinfo filename";
}
$buildinfo = abs_path($buildinfo);

my $temp_dir = tempdir(CLEANUP => 1);
cp( "/usr/lib/srebuild-hook", "$temp_dir/srebuild-hook" );
chdir $temp_dir;

my ( $cdata, $arch, $checksums, @depends ) = parse_buildinfo $buildinfo;

my $environ = $cdata->{"Environment"};
my @environ = ();
if ( defined($environ) ) {
    $environ =~ s/^\s+|\s+$//g;
    @environ = split /^/, $environ;
}

@environ = map {
    ( my $trimmed = $_ ) =~ s/^\s+|\s+$//g;
    $trimmed;
} @environ;

my $src_pkg = $cdata->{"Source"};
if ( not defined($src_pkg) ) {
    die "need Source field";
}

my $src_ver = $cdata->{"Version"};
if ( not defined($src_ver) ) {
    die "need Version field";
}

my $dtparser = DateTime::Format::Strptime->new(
    pattern  => '%a, %d %b %Y %H:%M:%S %z',
    on_error => 'croak',
);

my $builddate = $dtparser->parse_datetime( $cdata->{"Build-Date"} );
if ( not defined($builddate) ) {
    die "need Build-Date field";
}

my $build_path = $cdata->{"Build-Path"};
if ( not defined($build_path) ) {
    die "need Build-Path field";
}

my $first_seen =
    get_first_seen( $archive, $suite, $area, $arch, $src_pkg, $src_ver );

my $base_repo;

# If first_seen is defined, then the archive exists in some snapshot
# If not, we assume that it is a very new snapshot in sid.
# NOTE: This might fail in extremely rare edge cases (package neither in a
#       snapshot nor in sid)
if ( defined( $first_seen ) ) {
    $base_repo = "http://snapshot.debian.org/archive/$archive/$first_seen/";
} else {
    $base_repo = "http://deb.debian.org/$archive/";
}

@depends = filter_depends ( $base_repo, $suite, $area, $arch, @depends );

my @timestamps =
  generate_sources( $archive, $suite, $area, $arch, $builddate, @depends );

my $build_root =
  setup_chroot_sbuild( $base_repo, $suite, $area, $arch, $src_pkg, $src_ver, @timestamps );

print "architecture = $arch\n";

my $bn_buildinfo  = basename $buildinfo;
my $bn_build_root = basename $build_root;

# calculate absolute path because sbuild changes directories and the user
# should not be required to specify the absolute path on the command line

say STDOUT "starting prebuild";

system_fatal "mkdir", "--parent", "$build_root/tmp/";
cp( $buildinfo,        "$build_root/tmp/$bn_buildinfo" );
cp( $buildinfo,        "/tmp/$bn_buildinfo" );
cp( "/usr/lib/srebuild-hook", "$build_root/tmp/srebuild-hook" );
cp( "/usr/lib/srebuild-hook", "/tmp/srebuild-hook" );

say STDOUT "starting sbuild";

say STDOUT "resetting environment";

my %pres_env = %ENV;
undef %ENV;
foreach my $env (@environ) {
    $env =~ /^(.*)="(.*)"$/;
    $ENV{$1} = $2;
}

$build_path =~ m#^(.*)/([^/]+)$#;

system_fatal "sbuild", "--arch=$arch", "--dist=$suite",
  "--build-path=$1",
  "--no-apt-update", "--no-apt-upgrade", "--no-apt-distupgrade",
  "--chroot-setup-commands=/tmp/srebuild-hook chroot-setup /tmp/$bn_buildinfo",
"--starting-build-commands=/tmp/srebuild-hook starting-build /tmp/$bn_buildinfo",
  "--chroot=$bn_build_root", "${src_pkg}_${src_ver}";

say STDOUT "restoring environment";
%ENV = %pres_env;

say STDOUT "check new checksums";

check_checksums $checksums;

say STDOUT "package successfully rebuilt!";

my $lnk = generate_in_toto_metadata $checksums;
my $buildinfo_file = abs_path( (glob( "./*.buildinfo" ))[0] );

system_fatal "gpg", "--clearsign", $buildinfo_file;

system_fatal "ls";
$buildinfo_file = abs_path( (glob( "./*.asc" ))[0] );

say STDOUT $lnk;
say STDOUT $buildinfo_file;

system_fatal "cat", $lnk;
system_fatal "cat", $buildinfo_file;

foreach my $visualizer (@ARGV) {
  system_fatal "curl", "-fF", "--netrc-file", "/etc/srebuild-netrc", "metadata=\@$lnk", "-F", "buildinfo=\@$buildinfo_file", "$visualizer";
}

say STDOUT "everything is okay!";

unlink glob "/etc/schroot/chroot.d/$suite-$bn_build_root-$arch-sbuild-*";

say STDOUT "removing schroot entry"
